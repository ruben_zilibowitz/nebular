//
//  NBAppDelegate.h
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/05/2014.
//  Copyright (c) 2014 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
