//
//  NBRenderViewController.m
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/05/2014.
//  Copyright (c) 2014 Zilibowitz Productions. All rights reserved.
//

#import "NBRenderViewController.h"
#import <MessageUI/MessageUI.h>

const NSTimeInterval kUpdateInterval = 10;

@interface NBRenderViewController ()
@property (nonatomic,strong) NSTimer *updateTimer;
@end

@implementation NBRenderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self updateImage:nil];
    
    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:kUpdateInterval target:self selector:@selector(updateImage:) userInfo:nil repeats:YES];
    
    self.activityView.hidden = !self.isRenderingInProgress;
    self.renderInProgressLabel.hidden = !self.isRenderingInProgress;
}

- (void)updateImage:(NSTimer*)timer {
    CGImageRef imgRef = CGBitmapContextCreateImage(self.bitmapCtx);
    UIImage* img = [UIImage imageWithCGImage:imgRef scale:[[UIScreen mainScreen] scale] orientation:UIImageOrientationUp];
    CGImageRelease(imgRef);
    
    self.imageView.image = img;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSString *) applicationDocumentsDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

- (void)cannotSendMailAlert {
    [[[UIAlertView alloc] initWithTitle:@"Cannot send email" message:@"Your device is unable to send email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (IBAction)sendEmail:(id)sender {
    if (! [MFMailComposeViewController canSendMail]) {
        [self cannotSendMailAlert];
        return;
    }
    
    [self updateImage:nil];
    
    NSString *filename = @"NebularRender.png";
    NSData *pngData = UIImagePNGRepresentation(self.imageView.image);
    
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
    [mailVC setSubject:@"Shared Nebular Render"];
    [mailVC setMessageBody:@"I would like to share this Nebular render with you." isHTML:NO];
    [mailVC addAttachmentData:pngData mimeType:@"image/png" fileName:filename];
    [mailVC setMailComposeDelegate:self];
    
    [self presentViewController:mailVC animated:YES completion:nil];
}

- (NSUInteger)savedImagesCount {
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[self applicationDocumentsDirectory] error:nil];
    contents = [contents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.png'"]];
    return contents.count;
}

- (IBAction)shareViaITunes:(id)sender {
    NSString *filename = [self saveWithPrefix:@"NebularRender"];
    
    [[[UIAlertView alloc] initWithTitle:@"Saved" message:[NSString stringWithFormat:@"Your image '%@' can now be accessed via iTunes file sharing.", filename] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (NSString*)saveWithPrefix:(NSString*)prefix {
    [self updateImage:nil];
    
    const NSUInteger count = self.savedImagesCount;
    NSString *filename = [NSString stringWithFormat:@"%@-%lu.png", prefix, (unsigned long)count];
    NSString *path = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:filename];
    [UIImagePNGRepresentation(self.imageView.image) writeToFile:path atomically:YES];
    
    return filename;
}

#pragma mark - Navigation

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.updateTimer invalidate];
    self.updateTimer = nil;
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
