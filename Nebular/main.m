//
//  main.m
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/05/2014.
//  Copyright (c) 2014 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NBAppDelegate class]));
    }
}
