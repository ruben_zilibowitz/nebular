//
//  NebularViewController+Gestures.m
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/02/12.
//  Copyright (c) 2012 N.A. All rights reserved.
//

#import "NebularViewController+Gestures.h"
#import "Attractor.h"

extern GLfloat gAttractorVertexData[];

@implementation NebularViewController (Gestures)

- (void)setupGestures {
    UIPanGestureRecognizer *panRecogniser = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    panRecogniser.maximumNumberOfTouches = 1;
    [self.view addGestureRecognizer:panRecogniser];
    
    UIPanGestureRecognizer *doublePanRecogniser = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(doublePan:)];
    doublePanRecogniser.minimumNumberOfTouches = 2;
    [self.view addGestureRecognizer:doublePanRecogniser];
    
    UIPinchGestureRecognizer *pinchRecogniser = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
    [self.view addGestureRecognizer:pinchRecogniser];
    
    UIRotationGestureRecognizer *rotatationRecogniser = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotation:)];
    [self.view addGestureRecognizer:rotatationRecogniser];
    
    UITapGestureRecognizer *tapRecogniser = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    tapRecogniser.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:tapRecogniser];
}

- (void)stallTimer:(UIGestureRecognizer*)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded && self.updateTimer == nil) {
        [self resetTransitionTimer];
    }
    else if (gesture.state == UIGestureRecognizerStateBegan && self.updateTimer) {
        [self.updateTimer invalidate];
        self.updateTimer = nil;
    }
}

- (void)doublePan:(UIPanGestureRecognizer*)sender {
    CGPoint vel = [sender velocityInView:self.view];
    _translation.x += 0.0002*vel.x;
    _translation.y -= 0.0002*vel.y;
    
    _translation.x = fmaxf(_translation.x,-5);
    _translation.x = fminf(_translation.x,5);
    _translation.y = fmaxf(_translation.y,-5);
    _translation.y = fminf(_translation.y,5);
    
    [self stallTimer:sender];
}

- (void)pan:(UIPanGestureRecognizer*)sender {
    CGPoint vel = [sender velocityInView:self.view];
    _rotation.x += 0.0002*vel.y;
    _rotation.y += 0.0002*vel.x;
    
    [self stallTimer:sender];
}

- (void)pinch:(UIPinchGestureRecognizer*)sender {
    if (! isnan(sender.velocity)) {
        double s = exp(0.05*sender.velocity);
        _scale = fabsf(_scale * s);
        
        _scale = fmaxf(_scale, 0.1);
        _scale = fminf(_scale, 100);
    }
    
    [self stallTimer:sender];
}

- (void)rotation:(UIRotationGestureRecognizer*)sender {
    if (! isnan(sender.velocity)) {
        _rotation.z -= 0.01*sender.velocity;
    }
    
    [self stallTimer:sender];
}

- (void)doubleTap:(UITapGestureRecognizer*)sender {
    [self nextAttractor:nil];
    
    [self.updateTimer invalidate];
    self.updateTimer = nil;
    [self stallTimer:sender];
}

@end
