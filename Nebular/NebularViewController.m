//
//  NebularViewController.m
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/02/12.
//  Copyright (c) 2012 N.A. All rights reserved.
//

// nb: using left handed coordinate system. Right of screen is positive x direction.
// Top of screen is positive y.

#import "NebularViewController.h"
#import "NebularViewController+Gestures.h"
#import "Attractor.h"
#import "isPad.h"
#import "UIScreen+Retina.h"
#import "NBRenderViewController.h"

#define BUFFER_OFFSET(i) ((GLfloat *)NULL + (i))

#define ANTIALIASING        1
#define kNormalLineWidth    2
#define kRetinaLineWidth    4
#define kDefaultScale       3
#define kIPadNumNeedles     100000
#define kIPhoneNumNeedles   24000

const float kBrightness = 0.95;
const float kKey2FillRatio = 4;
const float explodeTime = 0.6;
const float explodePower = 0.5;

// Uniform index.
enum
{
    UNIFORM_EXPLODE,
    UNIFORM_EYE_VECTOR,
    UNIFORM_LIGHT_VECTORS,
    UNIFORM_LIGHT_COLORS,
    UNIFORM_OBJECT_MATRIX,
    UNIFORM_MODELVIEWPROJECTION_MATRIX,
    UNIFORM_NORMAL_MATRIX,
    NUM_UNIFORMS
};
GLint uniforms[NUM_UNIFORMS];

// Attribute index.
enum
{
    ATTRIB_VERTEX,
    ATTRIB_TANGENT,
    ATTRIB_COLOR,
    NUM_ATTRIBUTES
};

struct AttractorData {
    GLfloat *vertexData;
    GLfloat *tangentData;
    GLfloat *colorData;
    UInt32 numNeedles;
};

@interface NebularViewController () {
    GLuint _program;
    
    GLKVector3 _eyeVector;
    GLKMatrix4 _objectMatrix;
    GLKMatrix4 _modelViewProjectionMatrix;
    GLKMatrix3 _normalMatrix;
    
    GLfloat _explode;
    BOOL exploding, imploding;
    NSTimeInterval timeStamp;
    
    GLuint _vertexArray;
    GLuint _vertexBuffer;
    
    GLuint _directionArray;
    GLuint _directionBuffer;
    
    GLfloat _autoRotation;
    
    struct AttractorData attractorData;
    
    NSLock *attractorDataLock;
    
    BOOL niceStop;
    UInt16 niceWidth, niceHeight;
    CGFloat niceScale;
    CGContextRef niceBitmapCtx;
    dispatch_queue_t _renderQueue;
}
@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) GLKBaseEffect *effect;

@property (atomic,assign) UIBackgroundTaskIdentifier bgTask;
@property (atomic,assign) BOOL isRenderingHQ;

- (void)setupGL;
- (void)tearDownGL;

- (BOOL)loadShaders;
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;
@end

@implementation NebularViewController

@synthesize context = _context;
@synthesize effect = _effect;
@synthesize updateTimer;

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];

    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    attractorDataLock = [[NSLock alloc] init];
    
    attractorData.numNeedles = kIPadNumNeedles;//(isPad() ? kIPadNumNeedles : kIPhoneNumNeedles);
    attractorData.vertexData = calloc(attractorData.numNeedles * 6 * 3, sizeof(GLfloat));
    attractorData.tangentData = & attractorData.vertexData[attractorData.numNeedles * 6];
    attractorData.colorData = & attractorData.vertexData[attractorData.numNeedles * 6 * 2];
    
    attractor = [Attractor attractor];
    currentAttractor = attractor;
    [attractorDataLock lock];
    [attractor fillVertices:attractorData.vertexData
                   tangents:attractorData.tangentData
                     colors:attractorData.colorData
                      count:attractorData.numNeedles];
    [attractorDataLock unlock];
    
    [self setupGestures];
    [self setupGL];
    
    _renderQueue = dispatch_queue_create("com.zilibowitzproductions.nebular.renderQueue", NULL);
    self.bgTask = UIBackgroundTaskInvalid;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Welcome to Nebular" message:@"- Use pan/rotate/zoom gestures to rotate and scale object.\n\n- Double tap to transition to next object.\n\n- Or just sit back and enjoy the show." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)enterForeground:(NSNotification*)note {
    [self resetTransitionTimer];
}

- (void)viewDidUnload
{    
    [super viewDidUnload];
    
    free(attractorData.vertexData);
    
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
	self.context = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    
    [self loadShaders];
    
    glEnable(GL_DEPTH_TEST);
    
    glGenVertexArraysOES(1, &_vertexArray);
    glBindVertexArrayOES(_vertexArray);
    
    [attractorDataLock lock];
    glGenBuffers(1, &_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*attractorData.numNeedles*6*3, attractorData.vertexData, GL_STATIC_DRAW);
    [attractorDataLock unlock];
    
    glEnableVertexAttribArray(ATTRIB_VERTEX);
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(ATTRIB_TANGENT);
    glVertexAttribPointer(ATTRIB_TANGENT, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(attractorData.numNeedles*6));
    glEnableVertexAttribArray(ATTRIB_COLOR);
    glVertexAttribPointer(ATTRIB_COLOR, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(attractorData.numNeedles*6*2));
    
    glBindVertexArrayOES(0);
    
#if ANTIALIASING
    GLKView *view = (GLKView *)self.view;
    view.drawableMultisample = GLKViewDrawableMultisample4X;
#endif
    GLfloat lineWidth = ([[UIScreen mainScreen] isRetina] ? kRetinaLineWidth : kNormalLineWidth);
    glLineWidth (lineWidth);
    
    _lightPosition[0] = GLKVector3Make(-2, 1, 3);  // key light
    _lightColor[0] = GLKVector3MultiplyScalar(GLKVector3Make(1,1,1),kBrightness);
    
    _lightPosition[1] = GLKVector3Make(1.5, 0, 3);  // fill light
    _lightColor[1] = GLKVector3MultiplyScalar(GLKVector3Make(1,1,1),kBrightness/kKey2FillRatio);
    
    _lightPosition[2] = GLKVector3Make(-5, 1, -3);  // back light
    _lightColor[2] = GLKVector3MultiplyScalar(GLKVector3Make(1,1,1),kBrightness);
    
    _scale = kDefaultScale;
    
    _explode = 1.0;
    exploding = NO;
    imploding = YES;
    
    [self resetTransitionTimer];
    
//    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
    
    glDeleteBuffers(1, &_vertexBuffer);
    glDeleteVertexArraysOES(1, &_vertexArray);
    
    if (_program) {
        glDeleteProgram(_program);
        _program = 0;
    }
}

- (void)nextAttractor:(NSTimer*)timer {
    if (!imploding && !exploding) {
        exploding = YES;
        timeStamp = self.timeSinceLastResume;
        _explode = 0.0;
    }
}

- (void)nextAttractorFromStyle {
    NSInteger style = [[NSUserDefaults standardUserDefaults] integerForKey:@"attractor_style"];
    switch (style) {
        case kAttractorStyle_Preset:
            [attractor nextAttractor];
            break;
        case kAttractorStyle_Randomise:
            [attractor randomise];
            break;
        case kAttractorStyle_Both:
        {
            if (random() % 2)
                [attractor nextAttractor];
            else
                [attractor randomise];
        }
            break;
    }
}

- (void)fillVerticesOffline:(NSThread*)thread {
    currentAttractor = [attractor copy];
    [self nextAttractorFromStyle];
    [attractor randomiseGradient];
    [attractorDataLock lock];
    [attractor fillVertices:attractorData.vertexData
                   tangents:attractorData.tangentData
                     colors:attractorData.colorData
                      count:attractorData.numNeedles];
    [attractorDataLock unlock];
}

- (void)resetTransitionTimer {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults boolForKey:@"auto_transition"]) {
        NSInteger updateFrequency = [defaults integerForKey:@"update_frequency"];
        if (updateFrequency == 0) {
            updateFrequency = 15;
            [defaults setInteger:15 forKey:@"update_frequency"];
        }
        self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:updateFrequency target:self selector:@selector(nextAttractor:) userInfo:nil repeats:YES];
    }
}

#pragma mark - GLKView and GLKViewController delegate methods

- (float)autoRotateSpeed {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL enabled = [defaults boolForKey:@"auto_rotate"];
    if (enabled) {
        NSInteger speed = [defaults integerForKey:@"auto_rotate_speed"];
        float speedsArray[] = {1e-1,3e-1,5e-1};
        assert(speed >= 0 && speed <= 2);
        return speedsArray[speed];
    }
    else {
        return 0;
    }
}

- (void)update
{
    float aspect = fabsf(self.view.bounds.size.width / self.view.bounds.size.height);
    GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(65.0f), aspect, 0.1f, 100.0f);
    
    GLKMatrix4 modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -4.0f);
    
    _objectMatrix = GLKMatrix4Identity;
    _objectMatrix = GLKMatrix4Translate(_objectMatrix, _translation.x, _translation.y, _translation.z);
    _objectMatrix = GLKMatrix4Rotate(_objectMatrix,
                                     _autoRotation += ([self autoRotateSpeed] * self.timeSinceLastUpdate),
                                     1, 1, 1);
    _objectMatrix = GLKMatrix4Rotate(_objectMatrix, _rotation.z, 0.0f, 0.0f, 1.0f);
    _objectMatrix = GLKMatrix4Rotate(_objectMatrix, _rotation.y, 0.0f, 1.0f, 0.0f);
    _objectMatrix = GLKMatrix4Rotate(_objectMatrix, _rotation.x, 1.0f, 0.0f, 0.0f);
    _objectMatrix = GLKMatrix4Scale(_objectMatrix, _scale, _scale, _scale);
    
    _normalMatrix = GLKMatrix3InvertAndTranspose(GLKMatrix4GetMatrix3(_objectMatrix), NULL);
    
    _modelViewProjectionMatrix = GLKMatrix4Multiply(projectionMatrix, modelViewMatrix);
    
    _eyeVector = GLKVector3Negate(GLKMatrix4MultiplyVector3WithTranslation(modelViewMatrix,
                                                                           GLKVector3Make(0,0,0)));
    
    if (exploding) {
        NSTimeInterval dt = self.timeSinceLastResume - timeStamp;
        if (dt < explodeTime) {
            _explode = powf(dt/explodeTime,explodePower);
        }
        else {
            timeStamp = self.timeSinceLastResume;
            
            _translation = GLKVector3Make(0, 0, 0);
            _scale = kDefaultScale;
            
            [attractorDataLock lock];
            glBindVertexArrayOES(_vertexArray);
            glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*attractorData.numNeedles*6*3, attractorData.vertexData, GL_STATIC_DRAW);
            [attractorDataLock unlock];
            glBindVertexArrayOES(0);
            
            imploding = YES;
            exploding = NO;
        }
    }
    
    if (imploding) {
        NSTimeInterval dt = self.timeSinceLastResume - timeStamp;
        if (dt < explodeTime) {
            _explode = powf(1.0f - dt/explodeTime,explodePower);
        }
        else {
            _explode = -1.0;
            imploding = NO;
            exploding = NO;
            
            [NSThread detachNewThreadSelector:@selector(fillVerticesOffline:) toTarget:self withObject:nil];
        }
    }
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glBindVertexArrayOES(_vertexArray);
    
    // Render the object again with ES2
    glUseProgram(_program);
    
    glUniform1fv(uniforms[UNIFORM_EXPLODE], 1, &_explode);
    glUniform3fv(uniforms[UNIFORM_EYE_VECTOR], 1, _eyeVector.v);
    glUniform3fv(uniforms[UNIFORM_LIGHT_VECTORS], 3, _lightPosition[0].v);
    glUniform3fv(uniforms[UNIFORM_LIGHT_COLORS], 3, _lightColor[0].v);
    glUniformMatrix4fv(uniforms[UNIFORM_OBJECT_MATRIX], 1, 0, _objectMatrix.m);
    glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX], 1, 0, _modelViewProjectionMatrix.m);
    glUniformMatrix3fv(uniforms[UNIFORM_NORMAL_MATRIX], 1, 0, _normalMatrix.m);
    
    glDrawArrays(GL_LINES, 0, attractorData.numNeedles*2);
}

#pragma mark -  OpenGL ES 2 shader compilation

- (BOOL)loadShaders
{
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
    // Create shader program.
    _program = glCreateProgram();
    
    // Create and compile vertex shader.
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname]) {
        NSLog(@"Failed to compile vertex shader");
        return NO;
    }
    
    // Create and compile fragment shader.
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname]) {
        NSLog(@"Failed to compile fragment shader");
        return NO;
    }
    
    // Attach vertex shader to program.
    glAttachShader(_program, vertShader);
    
    // Attach fragment shader to program.
    glAttachShader(_program, fragShader);
    
    // Bind attribute locations.
    // This needs to be done prior to linking.
    glBindAttribLocation(_program, ATTRIB_VERTEX, "position");
    glBindAttribLocation(_program, ATTRIB_TANGENT, "tangent");
    glBindAttribLocation(_program, ATTRIB_COLOR, "color");
    
    // Link program.
    if (![self linkProgram:_program]) {
        NSLog(@"Failed to link program: %d", _program);
        
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (_program) {
            glDeleteProgram(_program);
            _program = 0;
        }
        
        return NO;
    }
    
    // Get uniform locations.
    uniforms[UNIFORM_EXPLODE] = glGetUniformLocation(_program, "explode");
    uniforms[UNIFORM_EYE_VECTOR] = glGetUniformLocation(_program, "eyeVector");
    uniforms[UNIFORM_LIGHT_VECTORS] = glGetUniformLocation(_program, "lightPosition");
    uniforms[UNIFORM_LIGHT_COLORS] = glGetUniformLocation(_program, "lightColor");
    uniforms[UNIFORM_OBJECT_MATRIX] = glGetUniformLocation(_program, "objectMatrix");
    uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX] = glGetUniformLocation(_program, "modelViewProjectionMatrix");
    uniforms[UNIFORM_NORMAL_MATRIX] = glGetUniformLocation(_program, "normalMatrix");
    
    // Release vertex and fragment shaders.
    if (vertShader) {
        glDetachShader(_program, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader) {
        glDetachShader(_program, fragShader);
        glDeleteShader(fragShader);
    }
    
    return YES;
}

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file
{
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source) {
        NSLog(@"Failed to load vertex shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}

- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validateProgram:(GLuint)prog
{
    GLint logLength, status;
    
    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

static CGContextRef createOffscreenContext(CGSize size, CGFloat scale) {
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL, size.width*scale, size.height*scale, 8, size.width*scale*4, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    CGContextScaleCTM(context, scale, scale);
    CGColorSpaceRelease(colorSpace);
    return context;
}

- (IBAction)grabNice:(id)sender {
    niceStop = YES;
    niceWidth = self.view.bounds.size.width;
    niceHeight = self.view.bounds.size.height;
    niceScale = [[UIScreen mainScreen] scale];
    
    dispatch_async(_renderQueue, ^{
        self.isRenderingHQ = YES;
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self.activity startAnimating];
            self.showRenderButton.enabled = YES;
        });
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"background_render"]) {
            UIApplication *app = [UIApplication sharedApplication];
            self.bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
                [app endBackgroundTask:self.bgTask];
                self.bgTask = UIBackgroundTaskInvalid;
            }];
        }
        
        niceStop = NO;
        if (niceBitmapCtx) {
            CGContextRelease(niceBitmapCtx);
        }
        niceBitmapCtx = createOffscreenContext(CGSizeMake(niceWidth, niceHeight), niceScale);
        Attractor *a_attractor = [currentAttractor copy];
        GLKMatrix4 a_objectMatrix = _objectMatrix;
        GLKMatrix4 a_modelViewProjectionMatrix = _modelViewProjectionMatrix;
        GLKVector3 a_eyeVector = _eyeVector;
        GLKVector3 a_lightPosition[kLightCount] = {_lightPosition[0], _lightPosition[1], _lightPosition[2]};
        GLKVector3 a_lightColor[kLightCount] = {_lightColor[0], _lightColor[1], _lightColor[2]};
        
        [a_attractor fillBuffer:niceBitmapCtx width:niceWidth height:niceHeight scale:niceScale objectMatrix:a_objectMatrix mvpMatrix:a_modelViewProjectionMatrix eyeVector:a_eyeVector lightPosition:a_lightPosition lightColor:a_lightColor stop:&niceStop];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self.activity stopAnimating];
            if (self.bgTask != UIBackgroundTaskInvalid) {
                [[UIApplication sharedApplication] endBackgroundTask:self.bgTask];
                self.bgTask = UIBackgroundTaskInvalid;
            }
        });
        
        self.isRenderingHQ = NO;
    });
}

- (IBAction)stopGrabbing:(id)sender {
    niceStop = YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showRender"]) {
        if (niceBitmapCtx == nil) {
            return;
        }
        
        NBRenderViewController *renderVC = segue.destinationViewController;
        renderVC.bitmapCtx = niceBitmapCtx;
        
        self.paused = YES;
        
        renderVC.isRenderingInProgress = self.isRenderingHQ;
    }
}

- (IBAction)unwind:(UIStoryboardSegue *)unwindSegue {
    self.paused = NO;
}

@end
