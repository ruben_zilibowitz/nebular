//
//  Attractor.h
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/02/12.
//  Copyright (c) 2012 N.A. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import "OpenGLCommon.h"

#define kNumAttractorCoefficients   30

@interface Attractor : NSObject <NSCopying> {
    double cs[kNumAttractorCoefficients];
    NSUInteger codeIndex;
}

+ (Attractor*)attractor;
- (void)fillVertices:(GLfloat*)vs tangents:(GLfloat*)ds colors:(GLfloat*)cols count:(UInt32)count;
- (void)fillBuffer:(CGContextRef)bitmapCtx width:(UInt16)width height:(UInt16)height scale:(GLfloat)scale objectMatrix:(GLKMatrix4)objectMatrix mvpMatrix:(GLKMatrix4)mvpMatrix eyeVector:(GLKVector3)eyeVector lightPosition:(GLKVector3[kLightCount])lightPosition lightColor:(GLKVector3[kLightCount])lightColor stop:(BOOL*)stop;
- (void)nextAttractor;
- (void)randomise;
- (void)randomiseGradient;

@property (nonatomic,assign) GLKVector3 centroid;
@property (nonatomic,assign) float radius;
@property (nonatomic,assign) Vector3D gradientStart;
@property (nonatomic,assign) Vector3D gradientEnd;

@end
