//
//  UIScreen+Retina.h
//  Nebular
//
//  Created by Ruben Zilibowitz on 14/03/12.
//  Copyright (c) 2012 N.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScreen (Retina)

- (BOOL)isRetina;

@end
