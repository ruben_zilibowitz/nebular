/*
 *  isPad.c
 *  NebularGL
 *
 *  Created by Ruben Henner Zilibowitz on 15/02/11.
 *  Copyright 2011 Ruben Henner Zilibowitz. All rights reserved.
 *
 */

#include "isPad.h"


BOOL isPad() {
#ifdef UI_USER_INTERFACE_IDIOM
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#else
    return NO;
#endif
}
