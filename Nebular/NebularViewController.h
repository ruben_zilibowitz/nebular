//
//  NebularViewController.h
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/02/12.
//  Copyright (c) 2012 N.A. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

#define kAutoUpdateFrequency    8.0

@class Attractor;

@interface NebularViewController : GLKViewController {
    Attractor *attractor;
    Attractor *currentAttractor;
    
    float       _scale;
    GLKVector3  _rotation;
    GLKVector3  _translation;
    GLKVector3  _lightPosition[kLightCount];
    GLKVector3  _lightColor[kLightCount];
}

@property (strong,nonatomic) NSTimer *updateTimer;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *activity;
@property (nonatomic,weak) IBOutlet UIButton *showRenderButton;

- (void)nextAttractor:(NSTimer*)timer;
- (void)resetTransitionTimer;

- (IBAction)grabNice:(id)sender;
- (IBAction)stopGrabbing:(id)sender;
- (IBAction)unwind:(UIStoryboardSegue *)unwindSegue;

@end
