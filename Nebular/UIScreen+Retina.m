//
//  UIScreen+Retina.m
//  Nebular
//
//  Created by Ruben Zilibowitz on 14/03/12.
//  Copyright (c) 2012 N.A. All rights reserved.
//

#import "UIScreen+Retina.h"

@implementation UIScreen (Retina)

- (BOOL)isRetina {
    return [self respondsToSelector:@selector(displayLinkWithTarget:selector:)] && (self.scale > 1.1f);
}

@end
