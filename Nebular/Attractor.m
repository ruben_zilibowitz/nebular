//
//  Attractor.m
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/02/12.
//  Copyright (c) 2012 N.A. All rights reserved.
//

#import "Attractor.h"
#import "Matrix3x3.h"

const double kTangentScale = 1.0e-4;
const double kMaxTangentLength = 1000;
const double kFavourShortNeedlesFactor = 4.0;  // bigger values give finer detail but take longer to compute
const double kColorBias = 2.0;
const Vector3D defaultStartPoint = {1e-3,1e-3,1e-3};

#define kTimeOutRandomise    3
#define kMinLyapunovExp      2e-1

// light attenuation constants
#define attC    0.05
#define attL    0.2
#define attQ    0.08

#define kShadowsEnabled     1   // whether to use shadow maps
#define kDrawShadowBuffer   -1  // use -1 if you want to draw the main view. Use a positive number to draw that shadow map.

const GLfloat kRadiusFactorBallRadius = 0.005f;
const float kShininess = 4.f;

const BOOL kFlipVertical = YES;

typedef struct
{
    double r;       // percent [0 - 1]
    double g;       // percent [0 - 1]
    double b;       // percent [0 - 1]
} RGB;

typedef struct
{
    double h;       // angle in degrees [0 - 360]
    double s;       // percent [0 - 1]
    double v;       // percent [0 - 1]
} HSV;

static RGB RGBfromHSV(HSV value) {
    double      hh, p, q, t, ff;
    long        i;
    RGB         rgbOut;
    
    if (value.s <= 0.0) // < is bogus, just shuts up warnings
    {
        if (isnan(value.h)) // value.h == NAN
        {
            rgbOut.r = value.v;
            rgbOut.g = value.v;
            rgbOut.b = value.v;
            return rgbOut;
        }
        
        // error - should never happen
        rgbOut.r = 0.0;
        rgbOut.g = 0.0;
        rgbOut.b = 0.0;
        return rgbOut;
    }
    
    hh = value.h;
    if(hh >= 360.0) hh = 0.0;
        hh /= 60.0;
        i = (long)hh;
        ff = hh - i;
        p = value.v * (1.0 - value.s);
        q = value.v * (1.0 - (value.s * ff));
        t = value.v * (1.0 - (value.s * (1.0 - ff)));
        
        switch(i)
    {
        case 0:
            rgbOut.r = value.v;
            rgbOut.g = t;
            rgbOut.b = p;
            break;
        case 1:
            rgbOut.r = q;
            rgbOut.g = value.v;
            rgbOut.b = p;
            break;
        case 2:
            rgbOut.r = p;
            rgbOut.g = value.v;
            rgbOut.b = t;
            break;
            
        case 3:
            rgbOut.r = p;
            rgbOut.g = q;
            rgbOut.b = value.v;
            break;
        case 4:
            rgbOut.r = t;
            rgbOut.g = p;
            rgbOut.b = value.v;
            break;
        case 5:
        default:
            rgbOut.r = value.v;
            rgbOut.g = p;
            rgbOut.b = q;
            break;
    }
    return rgbOut;
}

static HSV HSVMake(double h, double s, double v) {
    HSV hsv = {h,s,v};
    return hsv;
}

static Vector3D Vector3DFromRGB(RGB rgb) {
    return Vector3DMake(rgb.r, rgb.g, rgb.b);
}

@interface Attractor () {
    GLKVector3 *ZBuffer;
    GLKVector3 *accumulationBuffer;
    UInt32 *counterBuffer;
    GLfloat **shadowBuffers;
    GLKMatrix4 *lightMatrices;
}
+ (const NSArray*)codes;
@end

@implementation Attractor
@synthesize centroid, radius, gradientStart, gradientEnd;

+ (Attractor*)attractor {
    Attractor *at = [[Attractor alloc] init];
    return at;
}

- (id)init {
    if (self = [super init]) {
        codeIndex = 0;
        [self nextAttractor];
        self.gradientStart = Vector3DFromRGB(RGBfromHSV(HSVMake(0,1,1)));
        self.gradientEnd = Vector3DFromRGB(RGBfromHSV(HSVMake(50,1,1)));
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    Attractor *newAttractor = [[[self class] allocWithZone:zone] init];
    if(newAttractor) {
        for (int i = 0; i < kNumAttractorCoefficients; i++) {
            newAttractor->cs[i] = self->cs[i];
        }
        newAttractor->codeIndex = self->codeIndex;
        newAttractor.centroid = self.centroid;
        newAttractor.radius = self.radius;
        newAttractor.gradientStart = self.gradientStart;
        newAttractor.gradientEnd = self.gradientEnd;
    }
    return newAttractor;
}

- (void)map:(Vector3D*)point {
    const double px = point->x, py = point->y, pz = point->z;
    point->x = cs[0] + cs[1]*px + cs[2]*px*px + cs[3]*px*py + cs[4]*px*pz + cs[5]*py + cs[6]*py*py + cs[7]*py*pz + cs[8]*pz + cs[9]*pz*pz;
    point->y = cs[10] + cs[11]*px + cs[12]*px*px + cs[13]*px*py + cs[14]*px*pz + cs[15]*py + cs[16]*py*py + cs[17]*py*pz + cs[18]*pz + cs[19]*pz*pz;
    point->z = cs[20] + cs[21]*px + cs[22]*px*px + cs[23]*px*py + cs[24]*px*pz + cs[25]*py + cs[26]*py*py + cs[27]*py*pz + cs[28]*pz + cs[29]*pz*pz;
}

- (void)jacobian:(Vector3D*)point output:(Matrix3x3)jacobian {
    const double px = point->x, py = point->y, pz = point->z;
    jacobian[0] = cs[1]+2*cs[2]*px+cs[3]*py+cs[4]*pz;
    jacobian[3] = cs[3]*px+cs[5]+2*cs[6]*py+cs[7]*pz;
    jacobian[6] = cs[4]*px+cs[7]*py+cs[8]+2*cs[9]*pz;
    jacobian[1] = cs[11]+2*cs[12]*px+cs[13]*py+cs[14]*pz;
    jacobian[4] = cs[13]*px+cs[15]+2*cs[16]*py+cs[17]*pz;
    jacobian[7] = cs[14]*px+cs[17]*py+cs[18]+2*cs[19]*pz;
    jacobian[2] = cs[21]+2*cs[22]*px+cs[23]*py+cs[24]*pz;
    jacobian[5] = cs[23]*px+cs[25]+2*cs[26]*py+cs[27]*pz;
    jacobian[8] = cs[24]*px+cs[27]*py+cs[28]+2*cs[29]*pz;
}

static inline double sigmoid(double x) {
    //return M_2_PI * atan(x * M_PI_2);
    //return 2.0/(1.0 + exp(-2.0 * x)) - 1.0;
    return tanh(x);
    //return erf(sqrt(M_PI)*0.5*x);
    //return x / sqrt(1.0 + x*x);
}

-(void)step:(Vector3D*)point tangent:(Vector3D*)tangent jacobian:(Matrix3x3)jacobian {
    [self jacobian:point output:jacobian];
    [self map:point];
    Matrix3x3Vector3DMultiply(jacobian,tangent,tangent);
    
    double mag = Vector3DMagnitude(*tangent);
    double newMag = kMaxTangentLength * sigmoid(mag / kMaxTangentLength);
    *tangent = Vector3DScalarMultiply(newMag / mag, *tangent);
}

- (Vector3D)gradient:(double)grad {
    return Vector3DAdd(Vector3DScalarMultiply(1.0-grad, gradientStart), Vector3DScalarMultiply(grad, gradientEnd));
}

static GLfloat neb_quadrance(GLKVector3 vec) {
    return GLKVector3DotProduct(vec, vec);
}

static GLKVector3 neb_reflect(GLKVector3 I, GLKVector3 N) {
    // I - 2.0 * dot(N, I) * N
    const float NdotI = GLKVector3DotProduct(N, I);
    return GLKVector3Subtract(I, GLKVector3MultiplyScalar(N, 2.f*NdotI));
}

static GLKVector3 neb_clamp(GLKVector3 vec, float min, float max) {
    vec.x = MIN(MAX(vec.x,min),max);
    vec.y = MIN(MAX(vec.y,min),max);
    vec.z = MIN(MAX(vec.z,min),max);
    return vec;
}

static GLKVector3 neb_multiply(GLKVector3 A, GLKVector3 B) {
    A.x *= B.x;
    A.y *= B.y;
    A.z *= B.z;
    return A;
}

// Common techniques to improve shadow maps
// http://msdn.microsoft.com/en-us/library/windows/desktop/ee416324(v=vs.85).aspx

// Cascaded shadow maps
// http://msdn.microsoft.com/en-us/library/windows/desktop/ee416307(v=vs.85).aspx

void projectPointToShadowBuffer(UInt16 width, UInt16 height, const GLKVector3 *objectVector, const GLKMatrix4 *lightMatrix, int *outCol, int *outRow, GLfloat *outShadowBuffer, GLKVector3 *outShadowVector) {
    GLKVector3 shadowVector = GLKMatrix4MultiplyVector3WithTranslation(*lightMatrix, *objectVector);
    
    *outCol = (shadowVector.x*width*0.5f + width*0.5f + 0.5f);
    *outRow = (shadowVector.y*height*0.5f + height*0.5f + 0.5f);
    
    if (outShadowVector) {
        *outShadowVector = shadowVector;
    }
    
    if (outShadowBuffer) {
        if (0 <= *outCol && *outCol < width && 0 <= *outRow && *outRow < height) {
            const int idx = (*outRow)*width + *outCol;
            if (shadowVector.z > outShadowBuffer[idx]) {
                outShadowBuffer[idx] = shadowVector.z;
            }
        }
    }
}

- (void)fillBuffer:(CGContextRef)bitmapCtx width:(UInt16)width height:(UInt16)height scale:(GLfloat)scale objectMatrix:(GLKMatrix4)objectMatrix mvpMatrix:(GLKMatrix4)mvpMatrix eyeVector:(GLKVector3)eyeVector lightPosition:(GLKVector3[kLightCount])lightPosition lightColor:(GLKVector3[kLightCount])lightColor stop:(BOOL*)stop {
    Vector3D point, point1, tangent, tangentN;
    double magnitude, grad;
    Matrix3x3 jacobian;
    Vector3D color;
    Vector3D centroid_;
    double radius_;
    const int iterations = 1000;
    const size_t bytesPerRow = CGBitmapContextGetBytesPerRow(bitmapCtx);
    const size_t bitsPerPixel = CGBitmapContextGetBitsPerPixel(bitmapCtx);
    
    NSAssert(bitsPerPixel == 32, @"bits per pixel should be 32");
    
    // capture the orbit
	for (int i = 0; i < iterations; i++) {
		[self step:&point tangent:&tangent jacobian:jacobian];
    }
    
    // find centroid
    Vector3DSet(&centroid_, 0, 0, 0);
	for (int i = 0; i < iterations; i++) {
		[self step:&point tangent:&tangent jacobian:jacobian];
        centroid_ = Vector3DAdd(centroid_, point);
    }
    centroid_ = Vector3DScalarMultiply(1.0 / iterations, centroid_);
    self.centroid = GLKVector3Make(centroid_.x, centroid_.y, centroid_.z);
    
    // find radius
    radius_ = 0;
    for (int i = 0; i < iterations; i++) {
		[self step:&point tangent:&tangent jacobian:jacobian];
        magnitude = Vector3DMagnitude(Vector3DMinus(point, centroid_));
        if (magnitude > radius_) {
            radius_ = magnitude;
        }
    }
    self.radius = radius_;
    
    width *= scale;
    height *= scale;
    
    // allocate Z buffer
    ZBuffer = calloc(sizeof(GLKVector3), width*height);
    
    // init Z buffer
    for (int idx = 0; idx < width*height; idx++) {
        ZBuffer[idx] = GLKVector3Make(0, 0, -1000);
    }
    
    // allocate accumulation buffer
    accumulationBuffer = calloc(sizeof(GLKVector3), width*height);
    
    // init accumulation buffer
    for (int idx = 0; idx < width*height; idx++) {
        accumulationBuffer[idx] = GLKVector3Make(0, 0, 0);
    }
    
    // allocate counter buffer
    counterBuffer = calloc(sizeof(UInt32), width*height);
    
    // init counter buffer
    for (int idx = 0; idx < width*height; idx++) {
        counterBuffer[idx] = 0;
    }
    
    // allocate shadow buffers
    shadowBuffers = nil;
    if (kShadowsEnabled) {
        shadowBuffers = calloc(sizeof(GLfloat*), kLightCount);
        for (int i = 0; i < kLightCount; i++) {
            shadowBuffers[i] = calloc(sizeof(GLfloat), width*height);
            for (int idx = 0; idx < width*height; idx++) {
                shadowBuffers[i][idx] = -10000;
            }
        }
    }
    
    // allocate shadow buffer projection matrices
    lightMatrices = nil;
    if (kShadowsEnabled) {
        lightMatrices = calloc(sizeof(GLKMatrix4), kLightCount);
        for (int i = 0; i < kLightCount; i++) {
            GLKVector3 N = GLKVector3Normalize(lightPosition[i]);
            GLKVector3 UP = GLKVector3CrossProduct(N, GLKVector3Make(0, 1, 0));
            if (neb_quadrance(UP) < 0.01) {
                UP = GLKVector3CrossProduct(N, GLKVector3Make(1, 0, 0));
            }
            // look at matrix
            GLKMatrix4 lookAtM = GLKMatrix4MakeLookAt(lightPosition[i].x, lightPosition[i].y, lightPosition[i].z, 0, 0, 0, UP.x, UP.y, UP.z);
            lookAtM = GLKMatrix4Multiply(GLKMatrix4MakeScale(1, 1, -1), lookAtM);
            // ortho matrix
            GLfloat left = 1000;
            GLfloat right = -1000;
            GLfloat bottom = 1000;
            GLfloat top = -1000;
            GLfloat nearZ = 1000;
            GLfloat farZ = -1000;
            for (int i = 0; i < iterations; i++) {
                [self step:&point tangent:&tangent jacobian:jacobian];
                point1 = Vector3DScalarMultiply(1.0 / radius_, Vector3DMinus(point, centroid_));
                GLKVector3 vector = GLKVector3Make(point1.x, point1.y, point1.z);
                GLKVector3 objectVector = GLKMatrix4MultiplyVector3(objectMatrix, vector);
                GLKVector3 shadowVector = GLKMatrix4MultiplyVector3WithTranslation(lookAtM, objectVector);
                // update bounds
                left = MIN(left,shadowVector.x);
                right = MAX(right,shadowVector.x);
                bottom = MIN(bottom,shadowVector.y);
                top = MAX(top,shadowVector.y);
                nearZ = MIN(nearZ,shadowVector.z);
                farZ = MAX(farZ,shadowVector.z);
            }
            // outset bounds
            left -= 0.1;
            right += 0.1;
            bottom -= 0.1;
            top += 0.1;
            nearZ -= 0.1;
            farZ += 0.1;
            NSLog(@"ortho %f %f %f %f %f %f", left, right, bottom, top, nearZ, farZ);
            GLKMatrix4 orthoM = GLKMatrix4MakeOrtho(left, right, bottom, top, nearZ, farZ);
            NSLog(@"v %@", NSStringFromGLKVector3(GLKMatrix4MultiplyVector3WithTranslation(orthoM, GLKVector3Make(0, 0, nearZ))));
            NSLog(@"v %@", NSStringFromGLKVector3(GLKMatrix4MultiplyVector3WithTranslation(orthoM, GLKVector3Make(0, 0, farZ))));
            NSLog(@"v %@", NSStringFromGLKVector3(GLKMatrix4MultiplyVector3WithTranslation(orthoM, GLKVector3Make(0, 0, (nearZ+farZ)/2))));
            lightMatrices[i] = GLKMatrix4Multiply(orthoM, lookAtM);
        }
    }
    
    point = defaultStartPoint;
    Vector3DSet(&tangent,0,0,1);
    Matrix3x3SetIdentity(jacobian);
    
    const GLfloat epsilonRadius = radius_ * kRadiusFactorBallRadius;
    const GLfloat epsilonQuadrance = epsilonRadius*epsilonRadius;
    
    // get the pixel data
    UInt8 *pixelData = CGBitmapContextGetData(bitmapCtx);
    
    // generate data
    while (! (*stop)) {
		do {
            [self step:&point tangent:&tangent jacobian:jacobian];
            magnitude = Vector3DMagnitude(tangent) / kMaxTangentLength;
        } while (1.0 - magnitude < kFavourShortNeedlesFactor*drand48());
        
        point1 = Vector3DScalarMultiply(1.0 / radius_, Vector3DMinus(point, centroid_));
        tangentN = tangent; Vector3DNormalize(&tangentN);
        grad = fmin(kColorBias*magnitude, 1.0);
        color = [self gradient:grad];
        
        // apply transformations
        GLKVector3 vector = GLKVector3Make(point1.x, point1.y, point1.z);
        GLKVector3 objectVector = GLKMatrix4MultiplyVector3(objectMatrix, vector);
        GLKVector3 projectedVector = GLKMatrix4MultiplyVector3(mvpMatrix, objectVector);
        
        // update buffers
        {
            const int col = (projectedVector.x*100*scale + width*0.5f + 0.5f);
            const int row = (projectedVector.y*100*scale + height*0.5f + 0.5f);
            if (0 <= col && col < width && 0 <= row && row < height) {
                const int idx = row*width + col;
                
                // update Z buffer
                if (objectVector.z > ZBuffer[idx].z + epsilonRadius) {
                    ZBuffer[idx] = objectVector;
                    accumulationBuffer[idx] = GLKVector3Make(0, 0, 0);
                    counterBuffer[idx] = 0;
                }
                
                const GLKVector3 offset = GLKVector3Subtract(ZBuffer[idx], objectVector);
                const GLfloat quad = neb_quadrance(offset);
                if (quad < epsilonQuadrance) {
                    long byteIndex = (long)(bytesPerRow * (kFlipVertical ? height-1 - row : row)) + (long)(col * (bitsPerPixel / 8));
                    
                    UInt8 *red = &pixelData[byteIndex];
                    UInt8 *green = &pixelData[byteIndex + 1];
                    UInt8 *blue = &pixelData[byteIndex + 2];
                    UInt8 *alpha = &pixelData[byteIndex + 3];
                    
                    // do lighting computations
                    {
                        const GLKVector3 viewpointVector = GLKVector3Normalize(GLKVector3Subtract(eyeVector, objectVector));
                        
                        GLKVector3 totalLight = GLKVector3Make(0, 0, 0);
                        for (int i = 0; i < kLightCount; i++) {
                            
                            GLKVector3 lightVector = GLKVector3Subtract(lightPosition[i], objectVector);
                            GLKVector3 lightVectorNormalized = GLKVector3Normalize(lightVector);
                            GLKVector3 objectTangent = GLKVector3Make(tangentN.x, tangentN.y, tangentN.z);
                            GLKVector3 normal = GLKVector3CrossProduct(GLKVector3CrossProduct(objectTangent, lightVectorNormalized),objectTangent);
                            
                            // diffuse
                            float LdotN = ABS(GLKVector3DotProduct(lightVectorNormalized, normal));
                            GLKVector3 diffuseColor = GLKVector3Make(color.x, color.y, color.z);
                            GLKVector3 Idiff = GLKVector3MultiplyScalar(diffuseColor, LdotN);
//                            Idiff = neb_clamp(Idiff,0.f,1.f);
                            
                            // specular
                            GLKVector3 reflectedLightVector = GLKVector3Negate(neb_reflect(lightVectorNormalized,normal));
                            float RdotV = powf(ABS(GLKVector3DotProduct(reflectedLightVector,viewpointVector)),kShininess);
                            GLKVector3 specularColor = GLKVector3Make(1, 1, 1);
                            GLKVector3 Ispec = GLKVector3MultiplyScalar(specularColor, RdotV);
//                            Ispec = neb_clamp(Ispec, 0.0, 1.0);
                            
                            // attenuation
                            const float d = GLKVector3Length(lightVector);
                            const float attenuation = MIN(MAX(1.0 / (attC + attL*d + attQ*d*d), 0.f), 1.f);
                            
                            // shadows
                            if (kShadowsEnabled) {
                                int col, row;
                                GLKVector3 shadowVector;
                                
                                // insert to shadow buffer
                                projectPointToShadowBuffer(width, height, &objectVector, &lightMatrices[i], &col, &row, shadowBuffers[i], &shadowVector);
                                
                                // check if current point is in shadow
                                if (0 <= col && col < width && 0 <= row && row < height) {
                                    const int idx = row*width + col;
                                    if (kDrawShadowBuffer >= 0 && i == kDrawShadowBuffer) {
                                        byteIndex = (long)(bytesPerRow * (kFlipVertical ? height-1 - row : row)) + (long)(col * (bitsPerPixel / 8));
                                        
                                        UInt8 *red = &pixelData[byteIndex];
                                        UInt8 *green = &pixelData[byteIndex + 1];
                                        UInt8 *blue = &pixelData[byteIndex + 2];
                                        UInt8 *alpha = &pixelData[byteIndex + 3];
                                        
                                        *red = 0; *green = 0; *blue = 0;
                                        *alpha = 255;
                                    }
                                    
                                    if (shadowBuffers[i][idx] - shadowVector.z > radius_*0.02f) {
//                                        totalLight = GLKVector3Make(1-self.gradientStart.x, 1-self.gradientStart.y, 1-self.gradientStart.z);
                                        continue;
                                    }
                                }
                            }
                            
                            // result
                            totalLight = GLKVector3Add(totalLight, GLKVector3MultiplyScalar(neb_multiply(lightColor[i],GLKVector3Add(Idiff, Ispec)), attenuation));
                        }
                        
                        if (kDrawShadowBuffer < 0) {
                            // clamp total lighting result
                            totalLight = neb_clamp(totalLight, 0.0, 1.0);
                            
                            // add to accumulation buffer
                            accumulationBuffer[idx] = GLKVector3Add(accumulationBuffer[idx], totalLight);
                            counterBuffer[idx] ++;
                            
                            *red = (UInt8)(accumulationBuffer[idx].x / counterBuffer[idx] * 254.f + 0.5f);
                            *green = (UInt8)(accumulationBuffer[idx].y / counterBuffer[idx] * 254.f + 0.5f);
                            *blue = (UInt8)(accumulationBuffer[idx].z / counterBuffer[idx] * 254.f + 0.5f);
                            *alpha = 255;
                        }
                    }
                }
            }
        }
    }
    
    
    // free buffers
    free(ZBuffer);
    free(accumulationBuffer);
    free(counterBuffer);
    if (kShadowsEnabled) {
        for (int i = 0; i < kLightCount; i++) {
            free(shadowBuffers[i]);
        }
        free(shadowBuffers);
        free(lightMatrices);
    }
    
    ZBuffer = nil;
    accumulationBuffer = nil;
    counterBuffer = nil;
    shadowBuffers = nil;
    lightMatrices = nil;
}

- (void)fillVertices:(GLfloat*)vs tangents:(GLfloat*)ds colors:(GLfloat*)cols count:(UInt32)count {
    Vector3D point, point1, point2, tangent, tangentN;
    double magnitude, grad;
    Matrix3x3 jacobian;
    Vector3D color;
    Vector3D centroid_;
    double radius_;
    const int iterations = 1000;
    
    point = defaultStartPoint;
    Vector3DSet(&tangent,0,0,1);
    Matrix3x3SetIdentity(jacobian);
    
    // capture the orbit
	for (int i = 0; i < iterations; i++) {
		[self step:&point tangent:&tangent jacobian:jacobian];
    }
    
    // find centroid
    Vector3DSet(&centroid_, 0, 0, 0);
	for (int i = 0; i < iterations; i++) {
		[self step:&point tangent:&tangent jacobian:jacobian];
        centroid_ = Vector3DAdd(centroid_, point);
    }
    centroid_ = Vector3DScalarMultiply(1.0 / iterations, centroid_);
    self.centroid = GLKVector3Make(centroid_.x, centroid_.y, centroid_.z);
    
    // find radius
    radius_ = 0;
    for (int i = 0; i < iterations; i++) {
		[self step:&point tangent:&tangent jacobian:jacobian];
        magnitude = Vector3DMagnitude(Vector3DMinus(point, centroid_));
        if (magnitude > radius_) {
            radius_ = magnitude;
        }
    }
    self.radius = radius_;
    
    // generate data
    for (int i = 0; i < count; i++) {
		do {
            [self step:&point tangent:&tangent jacobian:jacobian];
            magnitude = Vector3DMagnitude(tangent) / kMaxTangentLength;
        } while (1.0 - magnitude < kFavourShortNeedlesFactor*drand48());
        
        point1 = Vector3DScalarMultiply(1.0 / radius_, Vector3DMinus(point, centroid_));
        point1 = Vector3DMinus(point1, Vector3DScalarMultiply(0.5*kTangentScale, tangent));
        point2 = Vector3DAdd(point1, Vector3DScalarMultiply(kTangentScale, tangent));
        vs[6*i] = point1.x; vs[6*i+1] = point1.y; vs[6*i+2] = point1.z;
        vs[6*i+3] = point2.x; vs[6*i+4] = point2.y; vs[6*i+5] = point2.z;
        tangentN = tangent; Vector3DNormalize(&tangentN);
        ds[6*i] = tangentN.x; ds[6*i+1] = tangentN.y; ds[6*i+2] = tangentN.z;
        ds[6*i+3] = tangentN.x; ds[6*i+4] = tangentN.y; ds[6*i+5] = tangentN.z;
        grad = fmin(kColorBias*magnitude, 1.0);
        color = [self gradient:grad];
        cols[6*i] = color.x; cols[6*i+1] = color.y; cols[6*i+2] = color.z;
        cols[6*i+3] = color.x; cols[6*i+4] = color.y; cols[6*i+5] = color.z;
    }
}

#define kLyapunovEstimateIterations 10000
#define kLyapunovEstimatePerturbationSize 1e-9

- (double) estimateLargestLyapunovExponent:(Vector3D)startPoint {
	const short iterations = kLyapunovEstimateIterations;
	const double peturbationSize = kLyapunovEstimatePerturbationSize;
    const double log2PeturbationSize = log2(peturbationSize);
	unsigned short i;
	Vector3D P, Q, QmP;
	double distance;
	double value = 0.0;
	
	// set starting state
    P = startPoint;
	// settle point
	for (i = 0; i < 100; i++)
		[self map:&P];
	// go
	Q = P;
	Q.z += peturbationSize;
	for (i = 0; i < iterations; i++) {
		if (isnan(value))
            return -2;
        if (Vector3DDotProduct(P, P) > 100.0)
			return -1;	// breached the boundary
		// advance both orbits
		[self map:&P];
		[self map:&Q];
		// multiply by rate of seperation
        QmP = Vector3DMinus(Q, P);
		distance = Vector3DMagnitude(QmP);
		value += log2(distance) - log2PeturbationSize;
		// adjust trajectory of peturbation orbit
        Q = Vector3DAdd(P, Vector3DScalarMultiply(peturbationSize/distance, QmP));
	}
    
	return value / iterations;
}

+ (const NSArray*)codes
{
    return [NSArray arrayWithObjects:
            //      @"IQCEPJOGFHNRQHJRLKKUGKOMRVGOLLM",
            //      @"IITJGMVLUNPPGUEPHQIPHHQMIEESDMO",
            //      @"IIGKDGHIFCGFINSHPIHOUCJETTIEFSU",
            //      @"INJVJLKKRTHJMDRQVPVGURHPVROILFC",
            //      @"IQDFKLQTIPQINQINWSSPNJTROQIOVQF",
            @"IFHTXHLVDKYSIPTUFJCFCLJGVQVHLYF",
            @"IMIGPMGPLQRHNVKLETWGMFESSIHUORQ",
            @"IFJLRNTKMSNJOXRDYPVEOOVTPLMGEAC",
            //      @"IGMNIKFCCNCPQFCJRQFUALCCLJPYVYD",
            //      @"IGNXQDPRVJPMBASUKJCRDRWVTDRQTTD",
            @"IGVQYNBNOMJCSSJIBFIDXWGGCWUOACV",
            @"IHHFGLDKRNJIYWJPMWNUOKJMLAAHQQD",
            //      @"IHJJDTICQIJETXFYNUSJKJSXGADBDKR",
            @"IHJNHTBGIFNDQMDOYWRYYTFSSRXQBEK",
            //      @"IHORHNHPPRVIVNNJUYHHMMJEWMMSUEU",
            //@"IIPPSGTMPCELDIPWPUPJCNQNFOBCYCK",
            @"IJEKESGYYFWLOGVKLMEWJMBKHSOIVTI",
            @"IJIFTNNCYIGTVRLCMNJLKUQDGJIQSJC",
            //      @"IJKRADSXGDBHIJTQJJDICEJKYSTXFNU",
            @"IJMUYUNOXHMLLOJREMMWFJRWTHSVXOL",
//            @"IJMYRKHSAUVRKPFVIJDMANDCIGJTIOB",
            @"IJVSURQUIINRDBRJAWRAKMLAHHUAOON",
            @"IKNXVQUEXYETWOOSGNSBDMHTMCPFLNG",
            //      @"IKRTYCFPFLTLSMOKRPEKMGPQHMUQYGY",
            @"ILITWKWUUUQIVQTIUWOLCNGELBMKGXC",
            //      @"ILNQAAYRNMTGVRVNPUNNKQFRBBTTYEW",
            @"ILRRHAEYWNTPWFLHTCSLYLFAKQITQTW",
            @"ILSQGYJJPISRVKJNPEXGIOSFDRQYGLO",
            @"ILURCEGOHOIQFJKBSNYGSNRUKKIKIHW",
            @"ILXRNRBKDJISQKAHRGWUTJONSIKVUBC",
            //      @"IMDBUAMMCSMTVFMONEEUDNEISFXDLCJ",
            //      @"IMROJCCRWSIMRPTLLENELIVYDEFWQHR",
            @"IMTISVBKHOIJFWSYEKEGYLWJKEOGVLM",
            @"IMYTXUJFERAEQWGBOYONOKBMHUVVXSX",
            @"INBSGKIRPRTKOEHRHUUGUHUQJNUSMOF",
            @"INDIJZIDVPJTZFFKYFLLJRPWFSHRKKU",
            @"INDVLLVRFLHPLLFCREMPLGLWNAFRPST",
            @"IOHGWFIHJPSGWTOJBXWJKPBLKFRUKKQ",
            @"IOLORGSFDQYLISYPSQGJJRGINXVKJPE",
            @"IOQRYLZUZAQKSKBLLHVHOLNWOMRLXMI",
            @"IPGPJYPMITFPTBEEDRWRTUGSXCJFTWE",
            @"IPWRRDUTWIKBCTXUMTHLELPHJPADMQC",
            //      @"IQCBIKKBNREKJEWFTMAJYFGIEBHLFEH",
            @"IRQEDYQNTRPNDHNWNOFJOOLNTEEBPSA",
            @"ISKKGLKSUNOQLCDKUOBPINHIDBKPOKR",
            //      @"ISNQBNOJFJWMPQDTTFNMNNOISRYCDUN",
            @"IUWECTGSXJFPTFYIGPJPMPRTEWTBEDR",
            @"IWDWOGDGWGORJOBTUHFQBPRNTCBYQHP",
            @"IWHVUQKFCDAJPIATNPSLSFPLLUUGMGM",
            
            //      @"IGHCMVFESUFJJGIUOHWFROOIGMGFBEC",
            @"IHESJWNBMQQEOYLGPPBXGNHTJTPNTUG",
            //      @"IIIQMSNRHWELIGAGIURCSRIWABJSESC",
            @"IIUWOTLCIVQNMKGLXCBGELWUUWUQKIT",
            @"IIXYMLIVIVWOAXXXLEHKDPICIGQUQGF",
            @"IJVSBDFVNDDWWNMDMHDOPCFNYRIYWIU",
            @"IJWWHJKPHMMKUVMKFSRHKJCYOISSQNB",
            @"IKUELCPYRWJFNDCNNRBVQKQREITYMIY",
            @"ILLMEVWJKOGMOIVHTISBKJGYYEFWSEK",
            @"ILOTMOQYJBPLDUWTSWJQDQJVAQLEDQF",
            @"IMNGCLHTMPFKYEQXNXVUETBDSSWOOGN",
            @"INJWFGVSOPUNATNJMNRWDQMFKIGMRSB",
            @"INKRCPBNOMEMVQQKSKYEIJOCQWEYOFP",
            @"INLJYYNNEIORHAKLKJKOVJFTFGGSMQY",
            @"INNRCKWREIASTBGRGPADGMGSHPKMPHU",
            //      @"IOCVGJFNYEVPTEQLASRSELPUHOTDBXP",
            //      @"IOGBGSHOUTDPTRFKCORFDLNKOSPNPHA",
            @"IPBMEFIUKEKPDTZEJMPXSJTUFZLFRJA",
            //      @"IPIIOOVNXNHPAUADBROXSSACJSXGMKX",
            //      @"IPNBJINWBKXSIISTQCVRQNUPKSCLTXS",
            @"IQETFNJNAHIINXFKUHXYHMTTBNJSIII",
            @"IQNBDVISXIPPLGVLRMKNCMORMJOCIHX",
            @"IRGOUVHFMIJQBAKEWDJOVQNUSGCNPDU",
            @"IRIVIMLQBPFVPSLIKHJNDSPMWMCBGMK",
            @"IUFPFQLVOLTUAVQYFLEVREPQLSNQRCD",
            @"IUIMPUSPSEJNDPKKENDVSEHCVWDVEGQ",
            @"IWUBBBVGSWOQFPMBKOPLQKUEIKHSVHM",
            nil];
};

- (void)nextAttractor {
    NSString *code = [[Attractor codes] objectAtIndex:codeIndex];
    for (int i = 1; i < code.length; i++) {
        cs[i-1] = ([code characterAtIndex:i] - 'M') * 0.1;
    }
    codeIndex = (codeIndex+1) % [Attractor codes].count;
}

- (void)randomise {
    NSDate *startDate = [NSDate date];
    BOOL inTime = NO;
    do {
        unsigned short i;
        for (i = 0; i < kNumAttractorCoefficients; i++) {
            //cs[i] = round((drand48() - 0.5)*20) * 0.1;	// chooses randomly from: [-1.0, -0.9, -0.8 ... 1.0]
            cs[i] = round((drand48() - 0.5)*2200) * 0.001;	// chooses randomly from: [-1.0, -0.999 ... 1.0]
        }
    } while (([self estimateLargestLyapunovExponent:defaultStartPoint] < kMinLyapunovExp) &&
             (inTime = ([startDate timeIntervalSinceNow] + kTimeOutRandomise > 0)));
    
    if (!inTime) {
        [self nextAttractor];
    }
}

- (void)randomiseGradient {
    const HSV hsv = HSVMake(drand48()*360,1.f-drand48()*0.2,1);
    
    self.gradientStart = Vector3DFromRGB(RGBfromHSV(hsv));
    self.gradientEnd = Vector3DFromRGB(RGBfromHSV(HSVMake(fmod(hsv.h + 60, 360),1.f-drand48()*0.2,1)));
}

@end
