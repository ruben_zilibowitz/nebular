//
//  NebularViewController+Gestures.h
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/02/12.
//  Copyright (c) 2012 N.A. All rights reserved.
//

#import "NebularViewController.h"

@interface NebularViewController (Gestures)

- (void)setupGestures;

@end
