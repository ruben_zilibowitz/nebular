//
//  NBRenderViewController.h
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/05/2014.
//  Copyright (c) 2014 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface NBRenderViewController : UIViewController <MFMailComposeViewControllerDelegate>
@property (nonatomic,assign) CGContextRef bitmapCtx;
@property (nonatomic,assign) BOOL isRenderingInProgress;

@property (nonatomic,weak) IBOutlet UIImageView *imageView;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *activityView;
@property (nonatomic,weak) IBOutlet UILabel *renderInProgressLabel;
- (IBAction)sendEmail:(id)sender;
- (IBAction)shareViaITunes:(id)sender;
@end
