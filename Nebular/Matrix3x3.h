/*
 *  Matrix3x3.h
 *  NebularGL
 *
 *  Created by Ruben Henner Zilibowitz on 5/02/11.
 *  Copyright 2011 Ruben Henner Zilibowitz. All rights reserved.
 *
 */

#import "OpenGLCommon.h"

typedef double Matrix3x3[9];

static inline void Matrix3x3Multiply(const Matrix3x3 m1, const Matrix3x3 m2, Matrix3x3 result)
{
    result[0] = m1[0] * m2[0] + m1[3] * m2[1] + m1[6] * m2[2];
    result[1] = m1[1] * m2[0] + m1[4] * m2[1] + m1[7] * m2[2];
    result[2] = m1[2] * m2[0] + m1[5] * m2[1] + m1[8] * m2[2];
    
    result[3] = m1[0] * m2[3] + m1[3] * m2[4] + m1[6] * m2[5];
    result[4] = m1[1] * m2[3] + m1[4] * m2[4] + m1[7] * m2[5];
    result[5] = m1[2] * m2[3] + m1[5] * m2[4] + m1[8] * m2[5];
    
    result[6] = m1[0] * m2[6] + m1[3] * m2[7] + m1[6] * m2[8];
    result[7] = m1[1] * m2[6] + m1[4] * m2[7] + m1[7] * m2[8];
    result[8] = m1[2] * m2[6] + m1[5] * m2[7] + m1[8] * m2[8];
}

static inline void Matrix3x3Vector3DMultiply(const Matrix3x3 m, const Vector3D *v, Vector3D *result) {
   const double vx = v->x, vy = v->y, vz = v->z;
   result->x = m[0] * vx + m[3] * vy + m[6] * vz;
   result->y = m[1] * vx + m[4] * vy + m[7] * vz;
   result->z = m[2] * vx + m[5] * vy + m[8] * vz;
}

static inline void Matrix3x3SetIdentity(Matrix3x3 matrix)
{
    matrix[0] = matrix[4] =  matrix[8] = 1.0;
    matrix[1] = 0;
    matrix[2] = 0;
    matrix[3] = 0;
    matrix[5] = 0;
    matrix[6] = 0;
    matrix[7] = 0;
}

static inline void Matrix3x3Copy(Matrix3x3 dst, const Matrix3x3 src) {
   dst[0] = src[0];
   dst[1] = src[1];
   dst[2] = src[2];
   dst[3] = src[3];
   dst[4] = src[4];
   dst[5] = src[5];
   dst[6] = src[6];
   dst[7] = src[7];
   dst[8] = src[8];
}
