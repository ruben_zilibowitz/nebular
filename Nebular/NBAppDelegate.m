//
//  NBAppDelegate.m
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/05/2014.
//  Copyright (c) 2014 Zilibowitz Productions. All rights reserved.
//

#import "NBAppDelegate.h"

@implementation NBAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application setStatusBarHidden:YES];
    
    // Set the application defaults
    [[NSUserDefaults standardUserDefaults] registerDefaults:
     @{
       @"auto_transition" : @(YES),
       @"auto_rotate" : @(YES),
       @"update_frequency" : @(15),
       @"auto_rotate_speed" : @(0),
       @"background_render" : @(YES),
       @"attractor_style" : @(kAttractorStyle_Both)
       }];
    
    application.idleTimerDisabled = YES;
    
    srandom((unsigned int)time(NULL));
    srand48(time(NULL));
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
