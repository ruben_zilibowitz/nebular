//
//  Shader.vsh
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/02/12.
//  Copyright (c) 2012 N.A. All rights reserved.
//

#define kLightCount     3

#define pi              3.14159265358979323846264338327950288
#define pi_2            1.57079632679489661923132169163975144

// larger values increase the hardness of edges around lighting shadows
#define shadowHardness   4.0

// larger values create smaller more focussed specular reflections
#define shininess       4.0

// must be between 0 and 1. The intensity of the specular reflections.
#define specularity     0.8

// light attenuation constants
#define attC    0.05
#define attL    0.2
#define attQ    0.08

attribute vec4 position;
attribute vec3 tangent;
attribute vec3 color;

varying lowp vec4 colorVarying;

uniform float explode;
uniform vec3 eyeVector;
uniform vec3 lightPosition[kLightCount];
uniform vec3 lightColor[kLightCount];
uniform mat4 objectMatrix;
uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

float sigmoid(float x) {
    return 2.0/(1.0 + exp(-2.0 * pi * x)) - 1.0;
}

void main()
{
    vec4 objectPosition = objectMatrix * position;
    vec3 objectTangent = normalize(normalMatrix * tangent);
    vec3 viewpointVector = normalize(eyeVector - objectPosition.xyz);
    float clampedExplode = clamp(explode,0.0,1.0);
    
    vec3 totalLight = vec3(0.0);
    for (int i = 0; i < kLightCount; i++) {
        
        vec3 lightVector = normalize(lightPosition[i] - objectPosition.xyz);
        vec3 normal = cross(cross(objectTangent, lightVector), objectTangent);
        
        // diffuse
        float LdotN = abs(dot(lightVector,normal));
        vec3 diffuseColor = color;
        vec3 Idiff = diffuseColor * LdotN;
        Idiff = clamp(Idiff, 0.0, 1.0);
        
        // specular
        vec3 reflectedLightVector = -reflect(lightVector,normal);
        float RdotV = pow(abs(dot(reflectedLightVector,viewpointVector)),shininess);
        vec3 specularColor = vec3(specularity);
        vec3 Ispec = specularColor * RdotV;
        Ispec = clamp(Ispec, 0.0, 1.0);
        
        // shadow
        float shadow = sigmoid(shadowHardness * max(0.0,dot(viewpointVector,normal)));
        
        // attenuation
        float d = length(lightPosition[i] - objectPosition.xyz);
        float attenuation = clamp(1.0 / (attC + attL*d + attQ*d*d), 0.0, 1.0);
        
        // result
        totalLight += attenuation*shadow*lightColor[i]*(Idiff + Ispec);
    }
    colorVarying = vec4((1.0-clampedExplode*clampedExplode*clampedExplode)*totalLight,1.0);
    
    gl_Position = modelViewProjectionMatrix * (objectPosition + vec4(tangent * (4.0*clampedExplode),0.0));
}
