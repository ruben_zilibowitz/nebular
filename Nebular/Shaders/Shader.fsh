//
//  Shader.fsh
//  Nebular
//
//  Created by Ruben Zilibowitz on 24/02/12.
//  Copyright (c) 2012 N.A. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
